<?php
/*
Template name: Page - Full Width
*/
get_header(); ?>
<?php do_action( 'flatsome_before_page' ); ?>
<div id="content" role="main" class="content-area">
<?php 
	$all_gallery_posts = Rest_Gallery::handle_gallery_posts_request();
	$all_gallery_taxonomies = Rest_Gallery::handle_gallery_taxonomy_request();
 ?>
 <!-- The "Select Industry" Wrapper -->
<div>
	<?php if(!empty($all_gallery_taxonomies)) : ?>
	<select class="categories">

		<option value="" class="categories-category" id="default-disabled-option" disabled selected>Choose an Industry</option>

	 	<?php foreach ($all_gallery_taxonomies as $i => $taxonomy) : ?>
	 		<option 
	 			value="<?php echo $taxonomy['name']; ?>" 
	 			class="categories-category" 
	 			data-filter="<?php echo $taxonomy['term_id']; ?>" 
	 			data-catid="<?php echo $taxonomy['term_id']; ?>"
	 		>
	 			<?php echo $taxonomy['name']; ?>
	 		</option>
		<?php endforeach; ?>
	 </select>
	 <?php endif; ?>
</div>



<!-- Gallery Grid Wrapper -->
<section id="grid-wrapper">
	<?php 

	if(!empty($all_gallery_posts)) :
		$themeCounter = [
			1 => 0,
			2 => 0,
			3 => 0,
		];
		foreach ($all_gallery_posts as $i => $gallery_post) :
			$data_cat_attr = "";
			if( !empty($gallery_post['categories'])){
				foreach ($gallery_post['categories'] as $categoryInfo) {
					$data_cat_attr .= $categoryInfo['cat_id'] .= ' ';
				}
			} else {
				$data_cat_attr = '';
			}
			$themeNumber = "No Theme Selected";
			switch ($gallery_post['theme']) {
				case '123_1':
					# code...
					$themeNumber = "1";
					$themeCounter[$themeNumber]++;
					break;
				case '123_2':
					# code...
					$themeNumber = "2";
					$themeCounter[$themeNumber]++;
					break;
				case '123_3':
					# code...
					$themeNumber = "3";
					$themeCounter[$themeNumber]++;
					break;
				default:
					# code...
					$themeNumber = null;
					break;
			}
	 ?>
		<div 
		data-catid="<?php echo $data_cat_attr; ?>"
		data-refurl="<?php echo $gallery_post['url'] ?>"
		class="grid-item">

			<h1 class="grid-item-title"><?php echo (!empty($themeNumber) ? $themeNumber : "?"); ?></h1>
			<img
			class="grid-item-image"
			srcset="<?php echo $gallery_post['image'] . "?w=1024&h=1024&fit=crop 1024w" ?>, <?php echo $gallery_post['image'] . "?w=640&h=640&fit=crop 640w" ?>, <?php echo $gallery_post['image'] . "?w=480&h=480&fit=crop 480w" ?>"
			src="<?php echo $gallery_post['image'] . "?w=640&h=640&fit=crop" ?>"
			sizes="(min-width: 36em) 33.3vw, 100vw">
		</div>
	<?php 
		endforeach;
	endif;
	 ?>
	</section>
	<!-- A splash page for when no posts are shown -->
	 <article class="grid-splash">
	 	<img src="<?php echo get_stylesheet_directory_uri() . '/assets/imgs/gallery-splash.jpg'; ?>" alt="">
	 </article>
</div>
<?php do_action( 'flatsome_after_page' ); ?>
<?php get_footer(); ?>