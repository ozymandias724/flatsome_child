<?php 

	/**
	 * Thing
	 */
	class Rest_Gallery
	{
		
		public static function handle_gallery_posts_request(){

			$url = 'http://worksfor.us/123gallery/wp-json/gallery-sites/v1/galleries';
			$request = wp_remote_get( $url );
			if( is_wp_error( $request ) ) {
				return false; // Bail early
			}
			$body = wp_remote_retrieve_body( $request );
			$gallery_posts = json_decode( $body, true );
			return $gallery_posts;
		}

		public static function handle_gallery_taxonomy_request()
		{
			$url = 'http://worksfor.us/123gallery/wp-json/gallery-sites/v1/galleries-taxonomies';
			$request = wp_remote_get( $url );
			if( is_wp_error( $request ) ) {
				return false; // Bail early
			}
			$body = wp_remote_retrieve_body( $request );
			$all_gallery_taxonomies = json_decode( $body, true );
			return $all_gallery_taxonomies;
		}
	}




 ?>