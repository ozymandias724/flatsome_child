module.exports = grunt => {
    // Load all grunt tasks matching the ['grunt-*', '@*/grunt-*'] patterns
    require('load-grunt-tasks')(grunt);
 
    grunt.initConfig({
    	sass: {
    	  dist: {
    	    files: {
    	      'build/css/build.css' : 'sass/main.scss',
    	    },
    	  },
    	},
    	concat: {
    	  options: {
    	    separator: ';\n',
    	  },
    	  banner: 
    	    '// ***********************\
    	    // * Works For Us Alpha *\
    	    // ***********************\
    	    \
    	    \
    	    ',
    	  dist: {
    	    src: [
                'js_reqs/jquery-1.12.4.js',
                'js_reqs/*.js',
                'js_pre/main.js'
            ],
    	    dest: 'build/js/build.js',
    	  },
    	},
    	uglify : {
    	  build : {
    	    files: {
    	      'build/js/build.js' : ['build/js/build.js'], 
    	    }
    	  }
    	},
    	watch: {
    	  sass: {
    	    files: ['sass/**/*.scss'],
    	    tasks: ['sass'],
    	    options: {
    	      livereload : 35729
    	    },
    	  },
    	  js: {
    	    files: ['js_pre/*.js'],
    	    tasks: ['concat'],
    	    options: {
    	      livereload : 35729
    	    },
    	  },
    	  php: {
    	    files: ['**/*.php'],
    	    options: {
    	      livereload : 35729
    	    },
    	  },
    	  options: {
    	    style: 'expanded',
    	    compass: true,
    	  },
    	},
    });

    grunt.registerTask('default', ['sass','concat', 'watch']);
};