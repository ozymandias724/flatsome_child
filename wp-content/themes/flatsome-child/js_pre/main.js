var Kit = {};
;(function ( $, Kit, window, document, undefined ) {
	$(document).ready(function(){
		
		Kit.Breakpoint = {
			name : '',
			_init : function(){
				$(window).on('resize', Kit.Breakpoint._resizeHander);
				// $(window).on('load', Kit.Breakpoint._loadHandler);
				$(document).ready(Kit.Breakpoint._loadHandler);
			},
			_loadHandler: function(){
				if( window.innerWidth > 1024 ){
					Kit.Breakpoint.name = 'desktop';
				}
				else if( window.innerWidth <= 1024 && window.innerWidth >= 640 ){
					Kit.Breakpoint.name = 'tablet';
				}
				else {
					Kit.Breakpoint.name = 'mobile';	
				}
			},
			_resizeHander : function(){
				if( window.innerWidth > 1024 && Kit.Breakpoint.name != 'desktop' ){
					Kit.Breakpoint.name = 'desktop';
					Kit.Breakpoint._dispatchEvent();
				}
				else if( window.innerWidth <= 1024 && window.innerWidth >= 640 && Kit.Breakpoint.name != 'tablet' ){
					Kit.Breakpoint.name = 'tablet';
					Kit.Breakpoint._dispatchEvent();
				}
				else if( window.innerWidth < 640 && Kit.Breakpoint.name != 'mobile' ){
					Kit.Breakpoint.name = 'mobile';
					Kit.Breakpoint._dispatchEvent();
				}		
			},
			_dispatchEvent : function(){
				$(document).trigger($.Event('breakpoint', {device: Kit.Breakpoint.name}));
			}
		}
		Kit.Breakpoint._init();


		// Handle the Grid + Selection Transitions
		Kit.GalleryGrid = {
			catSelectField : $('.categories'),
			catSelectOptions : $('.categories-category:not(#default-disabled-option)'),
			gridWrapper : $('#grid-wrapper'),
			gridItems : $('.grid-item'),
			splashPage : $('.grid-splash'),
			_filterGridItems : function(e){
				if ( Kit.GalleryGrid.catSelectField.val() != null ) {
					if( !Kit.GalleryGrid.splashPage.is(':hidden') ){
						Kit.GalleryGrid.splashPage.hide();
						Kit.GalleryGrid.gridWrapper.addClass('itemsShown');
					}
					var themeCounter = 0;
					var checkThemeFound = [
						null,
						null,
						null
					];
					var selectFilterID = $(this)[0].selectedOptions[0].dataset['catid'];	
					// for each grid item...
					for (var i = Kit.GalleryGrid.gridItems.length - 1; i >= 0; i--) {
						// store 'this'
						var gridItem = $(Kit.GalleryGrid.gridItems[i])[0];
						// store 'this' category ID
						var gridItemIDs = $(Kit.GalleryGrid.gridItems[i])[0].dataset['catid'];
						// if 'this' matches selected catID :
						if ( gridItemIDs.indexOf(selectFilterID) > -1 ) {
							// get 'this' theme number
							var gridItemTheme = $(gridItem).find('.grid-item-title')[0].innerHTML - 1;
							// store one grid item object per theme
							if( !checkThemeFound[gridItemTheme] ){
								checkThemeFound[gridItemTheme] = gridItem;
							}
						}
					}
					for (var counter = checkThemeFound.length - 1; counter >= 0; counter--) {
						// sort the grid items by theme (1 ,2, 3)
						$(checkThemeFound[counter]).prependTo(Kit.GalleryGrid.gridWrapper);
					}
					Kit.GalleryGrid.gridItems.removeClass('grid-item--fadeIn');
					setTimeout(function(){
						Kit.GalleryGrid.gridItems.removeClass('grid-item--toDisplay');
					}, 400);
					setTimeout(function(){
						for (var counter = checkThemeFound.length - 1; counter >= 0; counter--) {
							
							// TAKE A LOOK AT THIS GENIUS MOVE
							// preload the src & srcset for the images to be revealed:
							$(checkThemeFound[counter]).find('.grid-item-image').attr('src', $(checkThemeFound[counter]).find('.grid-item-image').data('src'));

							// reveal the 3
							$(checkThemeFound[counter]).addClass('grid-item--toDisplay');
							$(checkThemeFound[counter]).addClass('grid-item--fadeIn');
						}
					}, 400);
				} else if ( Kit.GalleryGrid.catSelectField.val() == null ) {
					Kit.GalleryGrid.gridItems.removeClass('grid-item--fadeIn');
					setTimeout(function(){
						Kit.GalleryGrid.gridItems.removeClass('grid-item--toDisplay');
					}, 400);
					setTimeout(function(){
						Kit.GalleryGrid._revealSplash();
					}, 400);
				}
			},
			_revealSplash : function(e){
				Kit.GalleryGrid.gridWrapper.removeClass('itemsShown');
				Kit.GalleryGrid.splashPage.show();
			},
			_init : function(){
				Kit.GalleryGrid.catSelectField.select2({
					placeholder : 'Choose your Industry',
					allowClear : true,
					width : 'resolve',
					containerCssClass: 'select2containerclass'
				});

				Kit.GalleryGrid.catSelectField.on('change', Kit.GalleryGrid._filterGridItems);
			}
		}
		Kit.GalleryGrid._init();



		// Handle the Modal, iFrame, and Resizer

		Kit.GalleryModal = {

			modalWindow : $('#modal'),
			iframeWrapper : $('.modal-iframewrapper'),
			iFrame : $('.modal-iframewrapper iframe'),
			resizer : $('#modal-iframewrapper-resizer'),
			resizerOptions : $('#modal-iframewrapper-resizer-labels label'),
			gridItem : $('.grid-item'),
			modalOverlay : $('#modal'),
			_doOpenModal : function(e){
				Kit.GalleryModal._doAdjustModal();
				Kit.GalleryModal.modalWindow.show();
				Kit.GalleryModal.iFrame.attr('src', $(this)[0].dataset['refurl']);
			},
			_doCloseModal : function(e){
				if( e.currentTarget == e.target){
					Kit.GalleryModal.iFrame.attr('src', '');
					Kit.GalleryModal.modalWindow.hide();
				} else {
					e.stopPropagation();
				}
			},
			didClickResizer : function(e){
				Kit.GalleryModal.iframeWrapper.removeClass('isMobile isTablet isDesktop');
				switch( e.target.id ) {
					case 'mobile':
					Kit.GalleryModal.iframeWrapper.addClass('isMobile');
					break;
					case 'tablet' :
					Kit.GalleryModal.iframeWrapper.addClass('isTablet');
					break;
					case 'desktop' :
					Kit.GalleryModal.iframeWrapper.addClass('isDesktop');
					default :
					break;
				}
			},
			_doAdjustModal : function(e){
				Kit.GalleryModal.iframeWrapper.removeClass('isMobile isTablet isDesktop');
				switch( Kit.Breakpoint.name ) {
					case 'mobile':
					Kit.GalleryModal.iframeWrapper.addClass('isMobile');
					Kit.GalleryModal.resizer.find('#mobile').trigger('click');
					break;
					case 'tablet' :
					Kit.GalleryModal.iframeWrapper.addClass('isTablet');
					Kit.GalleryModal.resizer.find('#tablet').trigger('click');
					break;
					case 'desktop' :
					Kit.GalleryModal.iframeWrapper.addClass('isDesktop');
					Kit.GalleryModal.resizer.find('#desktop').trigger('click');
					default :
					break;
				}
			},
			_init : function(){
				Kit.GalleryModal.gridItem.on('click', Kit.GalleryModal._doOpenModal);
				Kit.GalleryModal.modalWindow.on('click', Kit.GalleryModal._doCloseModal);
				Kit.GalleryModal.resizer.on('change', Kit.GalleryModal.didClickResizer);
				$(window).on('breakpoint load', Kit.GalleryModal._doAdjustModal);
			}
		}
		Kit.GalleryModal._init();

	});
})( jQuery, Kit, window, document );

