<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main><!-- #main -->

<?php 
if( is_page('gallery') ) :
 ?>
<div id="modal">
	<div name="modal-iframewrapper-resizer" id="modal-iframewrapper-resizer">
		<input type="radio" name="iframeresizer" id="mobile"></input>
		<input type="radio" name="iframeresizer" id="tablet"></input>
		<input type="radio" name="iframeresizer" id="desktop"></input>
		<div class="modal-iframewrapper-resizer-labels">
			<label for="mobile">Mobile</label>
			<label for="tablet">Tablet</label>
			<label for="desktop">Desktop</label>
		</div>
	</div>
	<div class="modal-iframewrapper isMobile">
		<iframe class="modal-iframewrapper-iframe" src="" frameborder="0"></iframe>
	</div>
</div>
<?php 
endif;
 ?>


<footer id="footer" class="footer-wrapper">

	<?php do_action('flatsome_footer'); ?>

</footer><!-- .footer-wrapper -->

</div><!-- #wrapper -->

<?php wp_footer(); ?>

</body>
</html>
