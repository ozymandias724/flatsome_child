<?php
// Add custom Theme Functions here
// 

	include 'classes/rest_api.php';

	function compile_gallery_reqs(){
		wp_register_style(
			'child_build_css', 
			get_stylesheet_directory_uri() . '/build/css/build.css', 
			array(), 
			filemtime(get_stylesheet_directory_uri() . '/build/css/build.css'),
			'all'
		);
		wp_enqueue_style('child_build_css');


		wp_register_script(
			'child_build_js', 
			get_stylesheet_directory_uri() . '/build/js/build.js', 
			array( 'jquery' ), 
			filemtime(get_stylesheet_directory_uri() . '/build/js/build.js'), 
			true
		);
		wp_enqueue_script('child_build_js');
		
	}
	add_action('init', 'compile_gallery_reqs');


?>